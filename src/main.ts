import Vue from 'vue'
import App from './App.vue'

var accounting = require('accounting');
var moment = require('moment');

Vue.config.productionTip = false

Vue.filter('moneda',function(value:string){
  return accounting.formatMoney(value);
})

Vue.filter('fecha',function(date:string){
  return moment(date).format('LL');
})

let vm = new Vue({
  render: h => h(App),
}).$mount('#app')
